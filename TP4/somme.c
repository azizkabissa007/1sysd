#include <stdio.h>



int main() {

  int i, num, n, arr[50], sum = 0, min, max;

  printf("Combien de nombres souhaitez-vous saisir (max. 50) ? ");

  scanf("%d", &n);

  if (n > 50) {

    n = 50;

  }

  for (i = 0; i < n; i++) {

    printf("Entrez le nombre %d : ", i + 1);

    scanf("%d", &num);

    arr[i] = num;

    sum += num;

  }

  min = arr[0];

  max = arr[0];

  for (i = 1; i < n; i++) {

    if (arr[i] < min) {

      min = arr[i];

    }

    if (arr[i] > max) {

      max = arr[i];

    }

  }

  printf("La somme des nombres est : %d\n", sum);

  printf("Le contenu du tableau est : ");

  for (i = 0; i < n; i++) {

    printf("%d ", arr[i]);

  }

  printf("\n");

  printf("Le plus petit nombre est : %d\n", min);

  printf("Le plus grand nombre est : %d\n", max);

  return 0;

}
